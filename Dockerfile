ARG DVER=latest
FROM docker.io/alpine:$DVER
LABEL maintainer="Duncan Bellamy <dunk@denkimushi.com>"
ARG APKVER

ENV CONFIG_PATH /etc/dnsrobocert/config.yml
ENV CERTS_PATH /etc/letsencrypt

COPY --chmod=755 run.sh /run.sh
WORKDIR /usr/local/bin
COPY --chmod=755 container-scripts/set-timezone.sh ./

RUN apk update \
&& apk upgrade --available --no-cache \
&& apk add --no-cache --upgrade docker py3-dnsrobocert$APKVER

CMD [ "/run.sh" ]
