# dnsrobocert
Alpine Linux based dockerfile for using [dnsrobocert](https://github.com/adferrand/dnsrobocert) in a container image.

[![Docker Pulls](https://img.shields.io/docker/pulls/a16bitsysop/dnsrobocert.svg?style=plastic)](https://hub.docker.com/r/a16bitsysop/dnsrobocert/)
[![Docker Stars](https://img.shields.io/docker/stars/a16bitsysop/dnsrobocert.svg?style=plastic)](https://hub.docker.com/r/a16bitsysop/dnsrobocert/)
[![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/a16bitsysop/dnsrobocert/latest?style=plastic)](https://hub.docker.com/r/a16bitsysop/dnsrobocert/)
[![Release Commit SHA](https://img.shields.io/badge/dynamic/json.svg?label=release%20commit%20SHA&style=plastic&color=orange&query=sha&url=https://gitlab.com/container-email/dnsrobocert/-/raw/main/badges.json)](https://gitlab.com/container-email/dnsrobocert/)

## GitLab
GitLab Repository: [https://gitlab.com/container-email/dnsrobocert](https://gitlab.com/container-email/dnsrobocert)

## Configuration
Configuration is identical to the official dnsrobocert docker image which compiles dnsrobocert from source.
Docs are [here](https://dnsrobocert.readthedocs.io/en/latest/index.html)

## Environment Variables
| Name     | Desription                                             | Default |
|----------|--------------------------------------------------------|---------|
| TIMEZONE | Timezone to use inside the container, eg Europe/London | unset   |

## Examples
**To run a container with certs in /opt/robocerts/mail, and the docker.sock mounted so restart container works**
```bash
docker container run -v /opt/robocerts/letsencrypt:/etc/letsencrypt \
-v /opt/robocerts/mail:/certs/mail \
-v /opt/robocerts/dnsrobocert-config.yml:/etc/dnsrobocert/config.yml \
-v /var/run/docker.sock:/var/run/docker.sock \
-d --name dnsrobocert a16bitsysop/dnsrobocert
```
